# 概要

携帯電話ネットワーク利用制限状況確認アプリのライセンス認証機能追加案件。  
スマホ等のIMEIを入力すると、キャリアのIMEI確認サイトに自動接続し、制限状況を取得するアプリにライセンス認証の機能を追加したい。  
ライセンスは回数制とし、上限に達した時点でアプリの動作を制限する。  
(100回分のライセンスを発行した場合、100回以降はライセンス切れダイアログを表示)  
複数拠点かつ複数台数のPCから使用するため、インターネット経由でのライセンス認証を行う。  
１回確認するごとにWebAPI経由でサーバに情報をアップし、レスポンスとして残回数を取得したい。  

# 依頼内容
「ライセンスID」と「ユーザー名」を送ると「キー」と「使用可能回数」が返ってくるWebAPI環境の構築＆開発と、クライアント側でWebAPIを使用可能にするクラスの開発。
(クライアントは.Net 4.6.2ベースのWindowsFormアプリ)  
サーバはAWS上に構築する。サーバOS、DB、FW等は今回指定なしなので任せます。  
正し、使用言語は今後のメンテを考えてJavaかC#を希望。  